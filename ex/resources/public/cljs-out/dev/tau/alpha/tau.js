// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.tau');
goog.require('cljs.core');
goog.require('tau.alpha.state');
goog.require('tau.alpha.call');
goog.require('tau.alpha.util');
goog.require('cljs.reader');
goog.require('cljs.pprint');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.tau._STAR_num_taus_STAR_ = (25);
tau.alpha.tau._STAR_tau_size_STAR_ = (10000);
tau.alpha.tau.unlocked = (0);
if((typeof tau !== 'undefined') && (typeof tau.alpha !== 'undefined') && (typeof tau.alpha.tau !== 'undefined') && (typeof tau.alpha.tau.db !== 'undefined')){
} else {
tau.alpha.tau.db = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
tau.alpha.tau.ace = (function tau$alpha$tau$ace(iab,idx,ov,nv){
return Atomics.compareExchange(iab,idx,ov,nv);
});
tau.alpha.tau.await$ = (function tau$alpha$tau$await(iab,idx,v,n){
return Atomics.wait(iab,idx,v,n);
});
tau.alpha.tau.astore = (function tau$alpha$tau$astore(iab,idx,n){
return Atomics.store(iab,idx,n);
});
tau.alpha.tau.awake = (function tau$alpha$tau$awake(iab,idx,n){
return Atomics.wake(iab,idx,n);
});
tau.alpha.tau.aload = (function tau$alpha$tau$aload(iab,idx){
return Atomics.load(iab,idx);
});
tau.alpha.tau.latch_token = cljs.core.hash.call(null,tau.alpha.state.id);
tau.alpha.tau.latchm = (function tau$alpha$tau$latchm(p__9240){
var map__9241 = p__9240;
var map__9241__$1 = ((((!((map__9241 == null)))?(((((map__9241.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9241.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9241):map__9241);
var iab = cljs.core.get.call(null,map__9241__$1,new cljs.core.Keyword(null,"iab","iab",-1540543938));
var idx = cljs.core.get.call(null,map__9241__$1,new cljs.core.Keyword(null,"idx","idx",1053688473));
var c = tau.alpha.tau.ace.call(null,iab,idx,tau.alpha.tau.unlocked,tau.alpha.tau.latch_token);
if(cljs.core.not_EQ_.call(null,c,tau.alpha.tau.unlocked)){
var ac = c;
while(true){
var x = tau.alpha.tau.ace.call(null,iab,idx,tau.alpha.tau.unlocked,tau.alpha.tau.latch_token);
if(cljs.core.not_EQ_.call(null,x,tau.alpha.tau.unlocked)){
tau.alpha.tau.await$.call(null,iab,idx,ac,Number.POSITIVE_INFINITY);
} else {
}

var acc = tau.alpha.tau.ace.call(null,iab,idx,tau.alpha.tau.unlocked,tau.alpha.tau.latch_token);
if(cljs.core.not_EQ_.call(null,acc,tau.alpha.tau.unlocked)){
var G__9243 = acc;
ac = G__9243;
continue;
} else {
return null;
}
break;
}
} else {
return null;
}
});
tau.alpha.tau.latchr = (function tau$alpha$tau$latchr(var_args){
var G__9245 = arguments.length;
switch (G__9245) {
case 2:
return tau.alpha.tau.latchr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return tau.alpha.tau.latchr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.tau.latchr.cljs$core$IFn$_invoke$arity$2 = (function (iab,idx){
return tau.alpha.tau.latchr.call(null,iab,idx,(10000));
});

tau.alpha.tau.latchr.cljs$core$IFn$_invoke$arity$3 = (function (iab,idx,n){
if((n <= (0))){
var c = tau.alpha.tau.ace.call(null,iab,idx,tau.alpha.tau.unlocked,tau.alpha.tau.latch_token);
if(cljs.core.not_EQ_.call(null,c,tau.alpha.tau.unlocked)){
return tau.alpha.tau.latchr.call(null,iab,idx,(n - (1)));
} else {
return null;
}
} else {
var c = tau.alpha.tau.ace.call(null,iab,idx,tau.alpha.tau.unlocked,tau.alpha.tau.latch_token);
if(cljs.core.not_EQ_.call(null,c,tau.alpha.tau.unlocked)){
tau.alpha.tau.await$.call(null,iab,idx,c,Number.POSITIVE_INFINITY);

return tau.alpha.tau.latchr.call(null,iab,idx);
} else {
return null;
}
}
});

tau.alpha.tau.latchr.cljs$lang$maxFixedArity = 3;

tau.alpha.tau.unlatchm = (function tau$alpha$tau$unlatchm(p__9247){
var map__9248 = p__9247;
var map__9248__$1 = ((((!((map__9248 == null)))?(((((map__9248.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9248.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9248):map__9248);
var iab = cljs.core.get.call(null,map__9248__$1,new cljs.core.Keyword(null,"iab","iab",-1540543938));
var idx = cljs.core.get.call(null,map__9248__$1,new cljs.core.Keyword(null,"idx","idx",1053688473));
if(cljs.core._EQ_.call(null,tau.alpha.tau.latch_token,tau.alpha.tau.aload.call(null,iab,(0)))){
tau.alpha.tau.astore.call(null,iab,idx,tau.alpha.tau.unlocked);

return tau.alpha.tau.awake.call(null,iab,idx,Number.POSITIVE_INFINITY);
} else {
return null;
}
});

/**
 * Protocol for adding for adding functionality for getting the ID of an object.
 * @interface
 */
tau.alpha.tau.IDable = function(){};

tau.alpha.tau._id = (function tau$alpha$tau$_id(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$IDable$_id$arity$1 == null))))){
return o.tau$alpha$tau$IDable$_id$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._id[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._id["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"IDable.-id",o);
}
}
}
});


/**
 * Protocol for functionality shared atoms
 * @interface
 */
tau.alpha.tau.ISharedAtom = function(){};

tau.alpha.tau._get_sab = (function tau$alpha$tau$_get_sab(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_sab$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_sab$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_sab[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_sab["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-sab",o);
}
}
}
});

tau.alpha.tau._get_int32a = (function tau$alpha$tau$_get_int32a(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_int32a$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_int32a$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_int32a[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_int32a["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-int32a",o);
}
}
}
});

tau.alpha.tau._get_ab = (function tau$alpha$tau$_get_ab(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_ab$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_ab$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_ab[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_ab["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-ab",o);
}
}
}
});

tau.alpha.tau._get_lock = (function tau$alpha$tau$_get_lock(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_lock$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_lock$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_lock[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_lock["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-lock",o);
}
}
}
});

tau.alpha.tau._locked_QMARK_ = (function tau$alpha$tau$_locked_QMARK_(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_locked_QMARK_$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_locked_QMARK_$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._locked_QMARK_[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._locked_QMARK_["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-locked?",o);
}
}
}
});

tau.alpha.tau._set = (function tau$alpha$tau$_set(o,a){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_set$arity$2 == null))))){
return o.tau$alpha$tau$ISharedAtom$_set$arity$2(o,a);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._set[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o,a);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._set["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o,a);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-set",o);
}
}
}
});

tau.alpha.tau._get_watches = (function tau$alpha$tau$_get_watches(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_watches$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_watches$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_watches[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_watches["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-watches",o);
}
}
}
});

tau.alpha.tau._set_validator_BANG_ = (function tau$alpha$tau$_set_validator_BANG_(o,f){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_set_validator_BANG_$arity$2 == null))))){
return o.tau$alpha$tau$ISharedAtom$_set_validator_BANG_$arity$2(o,f);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._set_validator_BANG_[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o,f);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._set_validator_BANG_["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o,f);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-set-validator!",o);
}
}
}
});

tau.alpha.tau._get_validator = (function tau$alpha$tau$_get_validator(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_validator$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_validator$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_validator[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_validator["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-validator",o);
}
}
}
});

tau.alpha.tau._set_error_handler_BANG_ = (function tau$alpha$tau$_set_error_handler_BANG_(o,f){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_set_error_handler_BANG_$arity$2 == null))))){
return o.tau$alpha$tau$ISharedAtom$_set_error_handler_BANG_$arity$2(o,f);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._set_error_handler_BANG_[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o,f);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._set_error_handler_BANG_["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o,f);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-set-error-handler!",o);
}
}
}
});

tau.alpha.tau._get_error_handler = (function tau$alpha$tau$_get_error_handler(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_get_error_handler$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_get_error_handler$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._get_error_handler[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._get_error_handler["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-get-error-handler",o);
}
}
}
});

tau.alpha.tau._blocking_deref = (function tau$alpha$tau$_blocking_deref(o){
if(((!((o == null))) && (!((o.tau$alpha$tau$ISharedAtom$_blocking_deref$arity$1 == null))))){
return o.tau$alpha$tau$ISharedAtom$_blocking_deref$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.tau._blocking_deref[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.tau._blocking_deref["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"ISharedAtom.-blocking-deref",o);
}
}
}
});

tau.alpha.tau.latch = (function tau$alpha$tau$latch(o){
if((o.buffer instanceof SharedArrayBuffer)){
return tau.alpha.tau.latchr.call(null,o,(0));
} else {
return tau.alpha.tau.latchr.call(null,tau.alpha.tau._get_int32a.call(null,o),(0));
}
});
tau.alpha.tau.unlatch = (function tau$alpha$tau$unlatch(o){
if((o.buffer instanceof SharedArrayBuffer)){
return tau.alpha.tau.unlatchm.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"iab","iab",-1540543938),o,new cljs.core.Keyword(null,"idx","idx",1053688473),(0)], null));
} else {
return tau.alpha.tau.unlatchm.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"iab","iab",-1540543938),tau.alpha.tau._get_int32a.call(null,o),new cljs.core.Keyword(null,"idx","idx",1053688473),(0)], null));
}
});
tau.alpha.tau.get_tau_error_handler = (function tau$alpha$tau$get_tau_error_handler(o){
return tau.alpha.tau._get_error_handler.call(null,o);
});
tau.alpha.tau.wake = (function tau$alpha$tau$wake(o){
var ia = tau.alpha.tau._get_int32a.call(null,o);
Atomics.store(ia,(1),(1));

Atomics.wake(ia,(1),Number.POSITIVE_INFINITY);

return Atomics.store(ia,(1),(0));
});
tau.alpha.tau.block = (function tau$alpha$tau$block(o){
var ia = tau.alpha.tau._get_int32a.call(null,o);
Atomics.store(ia,(1),(0));

Atomics.wait(ia,(1),(0));

return Atomics.load(ia,(1));
});
tau.alpha.tau.with_latch = (function tau$alpha$tau$with_latch(o,ia,afn){
tau.alpha.tau.latch.call(null,ia);

try{return afn.call(null);
}catch (e9250){var e = e9250;
var temp__5455__auto__ = tau.alpha.tau.get_tau_error_handler.call(null,o);
if(cljs.core.truth_(temp__5455__auto__)){
var efn = temp__5455__auto__;
return tau.alpha.call.call.call(null,efn,e);
} else {
throw e;
}
}finally {tau.alpha.tau.unlatch.call(null,ia);

tau.alpha.tau.wake.call(null,o);
}});
tau.alpha.tau.get_tau_validator = (function tau$alpha$tau$get_tau_validator(o){
return tau.alpha.tau._get_validator.call(null,o);
});
tau.alpha.tau.enc_ab = (function tau$alpha$tau$enc_ab(ar,s){
try{var new_length = s.length;
var old_index = Atomics.load(ar,(2));
var new_index = ((cljs.core._EQ_.call(null,(0),old_index))?((ar.length / (2)) | (0)):(0));
var write_point = ((cljs.core._EQ_.call(null,(0),new_index))?(4):(1));
cljs.core.doall.call(null,cljs.core.map_indexed.call(null,((function (new_length,old_index,new_index,write_point){
return (function (p1__9251_SHARP_,p2__9252_SHARP_){
return (ar[((p1__9251_SHARP_ + write_point) + new_index)] = p2__9252_SHARP_);
});})(new_length,old_index,new_index,write_point))
,cljs.core.array_seq.call(null,s)));

Atomics.store(ar,((write_point - (1)) + new_index),new_length);

Atomics.store(ar,(2),new_index);

return ar;
}catch (e9253){var e = e9253;
return cljs.core.println.call(null,"enc-ab failed");
}});
tau.alpha.tau.unc_ab = (function tau$alpha$tau$unc_ab(a){
try{var i = Atomics.load(a,(2));
var read_point = new cljs.core.PersistentArrayMap(null, 1, [(0),(4)], null).call(null,i,(1));
var l = Atomics.load(a,((read_point - (1)) + i));
var ca = (new Int32Array(a.slice((i + read_point),((i + read_point) + l))));
return ca;
}catch (e9254){var e = e9254;
cljs.core.println.call(null,"failed on unc-ab");

return cljs.core.println.call(null,"error:",e);
}});
tau.alpha.tau.update_val_with_latch = (function tau$alpha$tau$update_val_with_latch(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9260 = arguments.length;
var i__4532__auto___9261 = (0);
while(true){
if((i__4532__auto___9261 < len__4531__auto___9260)){
args__4534__auto__.push((arguments[i__4532__auto___9261]));

var G__9262 = (i__4532__auto___9261 + (1));
i__4532__auto___9261 = G__9262;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((4) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((4)),(0),null)):null);
return tau.alpha.tau.update_val_with_latch.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),argseq__4535__auto__);
});

tau.alpha.tau.update_val_with_latch.cljs$core$IFn$_invoke$arity$variadic = (function (meta,o,ia,f,args){
return tau.alpha.tau.with_latch.call(null,o,ia,(function (){
if(cljs.core.truth_(new cljs.core.Keyword(null,"ab","ab",-839573926).cljs$core$IFn$_invoke$arity$1(meta))){
var ov = tau.alpha.tau.unc_ab.call(null,ia);
var r = cljs.core.apply.call(null,f,ov,args);
tau.alpha.tau.enc_ab.call(null,ia,r);

return r;
} else {
var ov = tau.alpha.util.unc.call(null,ia);
var v = new cljs.core.Keyword(null,"val","val",128701612).cljs$core$IFn$_invoke$arity$1(ov);
var r = cljs.core.apply.call(null,f,v,args);
var temp__5455__auto___9263 = tau.alpha.tau.get_tau_validator.call(null,o);
if(cljs.core.truth_(temp__5455__auto___9263)){
var validator_9264 = temp__5455__auto___9263;
if(cljs.core.truth_(tau.alpha.call.call.call(null,validator_9264,r))){
tau.alpha.util.enc.call(null,ia,cljs.core.assoc.call(null,ov,new cljs.core.Keyword(null,"val","val",128701612),r));

cljs.core._notify_watches.call(null,o,ov,r);
} else {
throw (new Error(["Validator failed for value: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(r)].join('')));
}
} else {
tau.alpha.util.enc.call(null,ia,cljs.core.assoc.call(null,ov,new cljs.core.Keyword(null,"val","val",128701612),r));

cljs.core._notify_watches.call(null,o,ov,r);
}

return r;
}
}));
});

tau.alpha.tau.update_val_with_latch.cljs$lang$maxFixedArity = (4);

/** @this {Function} */
tau.alpha.tau.update_val_with_latch.cljs$lang$applyTo = (function (seq9255){
var G__9256 = cljs.core.first.call(null,seq9255);
var seq9255__$1 = cljs.core.next.call(null,seq9255);
var G__9257 = cljs.core.first.call(null,seq9255__$1);
var seq9255__$2 = cljs.core.next.call(null,seq9255__$1);
var G__9258 = cljs.core.first.call(null,seq9255__$2);
var seq9255__$3 = cljs.core.next.call(null,seq9255__$2);
var G__9259 = cljs.core.first.call(null,seq9255__$3);
var seq9255__$4 = cljs.core.next.call(null,seq9255__$3);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9256,G__9257,G__9258,G__9259,seq9255__$4);
});

tau.alpha.tau.add_watch_with_latch = (function tau$alpha$tau$add_watch_with_latch(o,ia,k,f){
tau.alpha.tau.with_latch.call(null,o,ia,(function (){
return tau.alpha.util.enc.call(null,ia,cljs.core.assoc_in.call(null,tau.alpha.util.unc.call(null,ia),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"watches","watches",-273097535),k], null),cljs.core.pr_str.call(null,f)));
}));

return o;
});
tau.alpha.tau.remove_watch_with_latch = (function tau$alpha$tau$remove_watch_with_latch(o,ia,k){
tau.alpha.tau.with_latch.call(null,o,ia,(function (){
return tau.alpha.util.enc.call(null,ia,cljs.core.update.call(null,tau.alpha.util.unc.call(null,ia),new cljs.core.Keyword(null,"watches","watches",-273097535),cljs.core.dissoc,k));
}));

return o;
});
tau.alpha.tau.set_validator_with_latch = (function tau$alpha$tau$set_validator_with_latch(o,ia,f){
tau.alpha.tau.with_latch.call(null,o,ia,(function (){
return tau.alpha.util.enc.call(null,ia,cljs.core.assoc.call(null,tau.alpha.util.unc.call(null,ia),new cljs.core.Keyword(null,"validator","validator",-1966190681),cljs.core.pr_str.call(null,f)));
}));

return o;
});
tau.alpha.tau.set_tau_validator_BANG_ = (function tau$alpha$tau$set_tau_validator_BANG_(t,f){
return tau.alpha.tau._set_validator_BANG_.call(null,t,f);
});
tau.alpha.tau.set_error_handler_with_latch = (function tau$alpha$tau$set_error_handler_with_latch(o,ia,f){
tau.alpha.tau.with_latch.call(null,o,ia,(function (){
return tau.alpha.util.enc.call(null,ia,cljs.core.assoc.call(null,tau.alpha.util.unc.call(null,ia),new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),cljs.core.pr_str.call(null,f)));
}));

return o;
});
tau.alpha.tau.set_tau_error_handler_BANG_ = (function tau$alpha$tau$set_tau_error_handler_BANG_(t,f){
return tau.alpha.tau._set_error_handler_BANG_.call(null,t,f);
});

/**
* @constructor
 * @implements {tau.alpha.tau.IDable}
 * @implements {cljs.core.IWatchable}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.IReset}
 * @implements {cljs.core.ISwap}
 * @implements {tau.alpha.tau.Object}
 * @implements {tau.alpha.tau.ISharedAtom}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IDeref}
 * @implements {cljs.core.IPrintWithWriter}
*/
tau.alpha.tau.Tau = (function (sab,int32a,oid,meta,validator,watches){
this.sab = sab;
this.int32a = int32a;
this.oid = oid;
this.meta = meta;
this.validator = validator;
this.watches = watches;
this.cljs$lang$protocol_mask$partition0$ = 2153938944;
this.cljs$lang$protocol_mask$partition1$ = 98306;
});
tau.alpha.tau.Tau.prototype.equiv = (function (other){
var self__ = this;
var this$ = this;
return cljs.core._equiv.call(null,this$,other);
});

tau.alpha.tau.Tau.prototype.toString = (function (){
var self__ = this;
var _ = this;
if(cljs.core.truth_(new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta).startsWith("#Tau"))){
var n = cljs.core.apply.call(null,cljs.core.str,cljs.core.drop_last.call(null,cljs.core.drop.call(null,(15),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta))));
return ["#Tau {:id ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n),"}"].join('');
} else {
return ["#Tau {:id ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta)),"}"].join('');
}
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$ = cljs.core.PROTOCOL_SENTINEL;

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_lock$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.int32a;
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_set$arity$2 = (function (o,a){
var self__ = this;
var o__$1 = this;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(self__.meta),new cljs.core.Keyword(null,"ab","ab",-839573926))){
tau.alpha.tau.enc_ab.call(null,self__.int32a,a);
} else {
tau.alpha.util.enc.call(null,self__.int32a,a);

}

return a;
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_watches$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return new cljs.core.Keyword(null,"watches","watches",-273097535).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,self__.int32a));
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_sab$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.sab;
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_int32a$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.int32a;
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_error_handler$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return new cljs.core.Keyword(null,"error-handler","error-handler",-484945776).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,self__.int32a));
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_ab$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.int32a;
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_set_validator_BANG_$arity$2 = (function (o,f){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.set_validator_with_latch.call(null,o__$1,self__.int32a,f);
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_blocking_deref$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
tau.alpha.tau.block.call(null,o__$1);

if(cljs.core.truth_(new cljs.core.Keyword(null,"ab","ab",-839573926).cljs$core$IFn$_invoke$arity$1(self__.meta))){
return tau.alpha.tau.unc_ab.call(null,tau.alpha.tau._get_int32a.call(null,o__$1));
} else {
return new cljs.core.Keyword(null,"val","val",128701612).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,tau.alpha.tau._get_int32a.call(null,o__$1)));

}
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_set_error_handler_BANG_$arity$2 = (function (o,f){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.set_error_handler_with_latch.call(null,o__$1,self__.int32a,f);
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_locked_QMARK_$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return cljs.core.not_EQ_.call(null,(0),tau.alpha.tau.aload.call(null,self__.int32a,(0)));
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$ISharedAtom$_get_validator$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return new cljs.core.Keyword(null,"validator","validator",-1966190681).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,self__.int32a));
});

tau.alpha.tau.Tau.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (o,writer,opts){
var self__ = this;
var o__$1 = this;
return cljs.core._write.call(null,writer,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(o__$1)].join(''));
});

tau.alpha.tau.Tau.prototype.cljs$core$IMeta$_meta$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.meta;
});

tau.alpha.tau.Tau.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return goog.getUid(this$__$1);
});

tau.alpha.tau.Tau.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (o,other){
var self__ = this;
var o__$1 = this;
return (o__$1 === other);
});

tau.alpha.tau.Tau.prototype.cljs$core$IReset$_reset_BANG_$arity$2 = (function (o,nv){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.update_val_with_latch.call(null,self__.meta,o__$1,self__.int32a,((function (o__$1){
return (function (_){
return nv;
});})(o__$1))
);
});

tau.alpha.tau.Tau.prototype.cljs$core$ISwap$_swap_BANG_$arity$2 = (function (o,f){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.update_val_with_latch.call(null,self__.meta,o__$1,self__.int32a,f);
});

tau.alpha.tau.Tau.prototype.cljs$core$ISwap$_swap_BANG_$arity$3 = (function (o,f,a){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.update_val_with_latch.call(null,self__.meta,o__$1,self__.int32a,f,a);
});

tau.alpha.tau.Tau.prototype.cljs$core$ISwap$_swap_BANG_$arity$4 = (function (o,f,a,b){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.update_val_with_latch.call(null,self__.meta,o__$1,self__.int32a,f,a,b);
});

tau.alpha.tau.Tau.prototype.cljs$core$ISwap$_swap_BANG_$arity$5 = (function (o,f,a,b,xs){
var self__ = this;
var o__$1 = this;
return cljs.core.apply.call(null,tau.alpha.tau.update_val_with_latch,self__.meta,o__$1,self__.int32a,f,a,b,xs);
});

tau.alpha.tau.Tau.prototype.cljs$core$IWatchable$_notify_watches$arity$3 = (function (o,old_val,new_val){
var self__ = this;
var o__$1 = this;
var temp__5455__auto__ = tau.alpha.tau._get_watches.call(null,o__$1);
if(cljs.core.truth_(temp__5455__auto__)){
var watches__$1 = temp__5455__auto__;
var seq__9265 = cljs.core.seq.call(null,watches__$1);
var chunk__9266 = null;
var count__9267 = (0);
var i__9268 = (0);
while(true){
if((i__9268 < count__9267)){
var vec__9269 = cljs.core._nth.call(null,chunk__9266,i__9268);
var key = cljs.core.nth.call(null,vec__9269,(0),null);
var f = cljs.core.nth.call(null,vec__9269,(1),null);
tau.alpha.call.call.call(null,f,key,o__$1,old_val,new_val);


var G__9275 = seq__9265;
var G__9276 = chunk__9266;
var G__9277 = count__9267;
var G__9278 = (i__9268 + (1));
seq__9265 = G__9275;
chunk__9266 = G__9276;
count__9267 = G__9277;
i__9268 = G__9278;
continue;
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__9265);
if(temp__5457__auto__){
var seq__9265__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__9265__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__9265__$1);
var G__9279 = cljs.core.chunk_rest.call(null,seq__9265__$1);
var G__9280 = c__4351__auto__;
var G__9281 = cljs.core.count.call(null,c__4351__auto__);
var G__9282 = (0);
seq__9265 = G__9279;
chunk__9266 = G__9280;
count__9267 = G__9281;
i__9268 = G__9282;
continue;
} else {
var vec__9272 = cljs.core.first.call(null,seq__9265__$1);
var key = cljs.core.nth.call(null,vec__9272,(0),null);
var f = cljs.core.nth.call(null,vec__9272,(1),null);
tau.alpha.call.call.call(null,f,key,o__$1,old_val,new_val);


var G__9283 = cljs.core.next.call(null,seq__9265__$1);
var G__9284 = null;
var G__9285 = (0);
var G__9286 = (0);
seq__9265 = G__9283;
chunk__9266 = G__9284;
count__9267 = G__9285;
i__9268 = G__9286;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});

tau.alpha.tau.Tau.prototype.cljs$core$IWatchable$_add_watch$arity$3 = (function (o,key,f){
var self__ = this;
var o__$1 = this;
tau.alpha.tau.add_watch_with_latch.call(null,o__$1,self__.int32a,key,f);

return o__$1;
});

tau.alpha.tau.Tau.prototype.cljs$core$IWatchable$_remove_watch$arity$2 = (function (o,key){
var self__ = this;
var o__$1 = this;
return tau.alpha.tau.remove_watch_with_latch.call(null,o__$1,self__.int32a,key);
});

tau.alpha.tau.Tau.prototype.cljs$core$IDeref$_deref$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
if(cljs.core.truth_(new cljs.core.Keyword(null,"ab","ab",-839573926).cljs$core$IFn$_invoke$arity$1(self__.meta))){
return tau.alpha.tau.unc_ab.call(null,tau.alpha.tau._get_int32a.call(null,o__$1));
} else {
return new cljs.core.Keyword(null,"val","val",128701612).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,tau.alpha.tau._get_int32a.call(null,o__$1)));

}
});

tau.alpha.tau.Tau.prototype.tau$alpha$tau$IDable$ = cljs.core.PROTOCOL_SENTINEL;

tau.alpha.tau.Tau.prototype.tau$alpha$tau$IDable$_id$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return self__.oid;
});

tau.alpha.tau.Tau.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"sab","sab",2063101620,null),new cljs.core.Symbol(null,"int32a","int32a",628478350,null),new cljs.core.Symbol(null,"oid","oid",871839193,null),new cljs.core.Symbol(null,"meta","meta",-1154898805,null),new cljs.core.Symbol(null,"validator","validator",-325659154,null),new cljs.core.Symbol(null,"watches","watches",1367433992,null)], null);
});

tau.alpha.tau.Tau.cljs$lang$type = true;

tau.alpha.tau.Tau.cljs$lang$ctorStr = "tau.alpha.tau/Tau";

tau.alpha.tau.Tau.cljs$lang$ctorPrWriter = (function (this__4192__auto__,writer__4193__auto__,opt__4194__auto__){
return cljs.core._write.call(null,writer__4193__auto__,"tau.alpha.tau/Tau");
});

/**
 * Positional factory function for tau.alpha.tau/Tau.
 */
tau.alpha.tau.__GT_Tau = (function tau$alpha$tau$__GT_Tau(sab,int32a,oid,meta,validator,watches){
return (new tau.alpha.tau.Tau(sab,int32a,oid,meta,validator,watches));
});

tau.alpha.tau.wait = (function tau$alpha$tau$wait(o){
return tau.alpha.tau._blocking_deref.call(null,o);
});
tau.alpha.tau.notify_watches = (function tau$alpha$tau$notify_watches(o,ov,nv){
return cljs.core._notify_watches.call(null,o,ov,nv);
});
tau.alpha.tau.swap = (function tau$alpha$tau$swap(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9290 = arguments.length;
var i__4532__auto___9291 = (0);
while(true){
if((i__4532__auto___9291 < len__4531__auto___9290)){
args__4534__auto__.push((arguments[i__4532__auto___9291]));

var G__9292 = (i__4532__auto___9291 + (1));
i__4532__auto___9291 = G__9292;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((2) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((2)),(0),null)):null);
return tau.alpha.tau.swap.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4535__auto__);
});

tau.alpha.tau.swap.cljs$core$IFn$_invoke$arity$variadic = (function (o,f,a){
return cljs.core.apply.call(null,cljs.core._swap_BANG_,o,f,a);
});

tau.alpha.tau.swap.cljs$lang$maxFixedArity = (2);

/** @this {Function} */
tau.alpha.tau.swap.cljs$lang$applyTo = (function (seq9287){
var G__9288 = cljs.core.first.call(null,seq9287);
var seq9287__$1 = cljs.core.next.call(null,seq9287);
var G__9289 = cljs.core.first.call(null,seq9287__$1);
var seq9287__$2 = cljs.core.next.call(null,seq9287__$1);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9288,G__9289,seq9287__$2);
});

tau.alpha.tau.get_next_ctr = (function tau$alpha$tau$get_next_ctr(sab){
var ia = (new Int32Array(sab,(0),(1)));
var n = tau.alpha.tau.aload.call(null,ia,(0));
var old_n = n;
var new_n = ((((old_n + (1)) < tau.alpha.tau._STAR_num_taus_STAR_))?(old_n + (1)):(2));
tau.alpha.tau.ace.call(null,ia,(0),old_n,new_n);

return new_n;
});
tau.alpha.tau.get_next_ia_idx = (function tau$alpha$tau$get_next_ia_idx(n){
return (((n * (2)) * (4)) * tau.alpha.tau._STAR_tau_size_STAR_);
});
tau.alpha.tau.construct_tau = (function tau$alpha$tau$construct_tau(sab,nid){
var tid = tau.alpha.util.read.call(null,nid);
var n1 = cljs.core.nth.call(null,cljs.core.name.call(null,tid),(2));
if(cljs.core._EQ_.call(null,n1,"_")){
var ab_QMARK_ = cljs.core._EQ_.call(null,"_",cljs.core.nth.call(null,cljs.core.name.call(null,tid),(3)));
var size = (sab.byteLength / (4));
var ia = (new Int32Array(sab,(0),size));
var meta = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join(''),new cljs.core.Keyword(null,"size","size",1098693007),size,new cljs.core.Keyword(null,"ab","ab",-839573926),ab_QMARK_], null);
var t = (new tau.alpha.tau.Tau(sab,ia,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join(''),meta,null,null));
cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t)].join(''),t);

return t;
} else {
var n2 = cljs.core.nth.call(null,cljs.core.name.call(null,tid),(3));
var n = tau.alpha.util.read.call(null,((cljs.core._EQ_.call(null,n1,"0"))?n2:[cljs.core.str.cljs$core$IFn$_invoke$arity$1(n1),cljs.core.str.cljs$core$IFn$_invoke$arity$1(n2)].join('')));
var idx = tau.alpha.tau.get_next_ia_idx.call(null,n);
var sab__$1 = new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs));
var ia = (new Int32Array(sab__$1,idx,((2) * tau.alpha.tau._STAR_tau_size_STAR_)));
var t = (new tau.alpha.tau.Tau(sab__$1,ia,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join(''),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join('')], null),null,null));
cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t)].join(''),t);

return t;
}
});
tau.alpha.tau.receive_tau = (function tau$alpha$tau$receive_tau(sab,nid){
var tau_id = ["#Tau {:id ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(nid),"}"].join('');
var atau = tau.alpha.tau.construct_tau.call(null,sab,nid);
return cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,tau_id,atau);
});
tau.alpha.tau.send_tau = (function tau$alpha$tau$send_tau(port,sab,nid){
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
return tau.alpha.io.do_on.call(null,port,(function (sab__$1,nid__$1){
return tau.alpha.tau.receive_tau.call(null,sab__$1,nid__$1);
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.println], null),sab,nid);
} else {
return tau.alpha.io.do_on.call(null,"screen",(function (sab__$1,port__$1,nid__$1){
return tau.alpha.io.do_on.call(null,port__$1,(function (sab__$2,nid__$2){
return tau.alpha.tau.receive_tau.call(null,sab__$2,nid__$2);
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.println], null),sab__$1,nid__$1);
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.println], null),sab,port,nid);
}
});
tau.alpha.tau.send_tau_to_screen = (function tau$alpha$tau$send_tau_to_screen(sab,nid){
return tau.alpha.io.do_on.call(null,"screen",(function (sab__$1,nid__$1){
return cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,["#Tau {:id ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(nid__$1),"}"].join(''),tau.alpha.tau.construct_tau.call(null,sab__$1,nid__$1));
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.println], null),sab,nid);
});
tau.alpha.tau.dist_tau = (function tau$alpha$tau$dist_tau(t){
if(cljs.core.not.call(null,tau.alpha.state.on_screen_QMARK_.call(null))){
tau.alpha.tau.send_tau_to_screen.call(null,tau.alpha.tau._get_sab.call(null,t),tau.alpha.tau._id.call(null,t));
} else {
}

return cljs.core.doall.call(null,cljs.core.map.call(null,(function (p1__9293_SHARP_){
return tau.alpha.tau.send_tau.call(null,p1__9293_SHARP_,tau.alpha.tau._get_sab.call(null,t),tau.alpha.tau._id.call(null,t));
}),cljs.core.keys.call(null,cljs.core.deref.call(null,tau.alpha.state.ports))));
});
tau.alpha.tau.send_taus = (function tau$alpha$tau$send_taus(port){
var ts = cljs.core.vals.call(null,cljs.core.deref.call(null,tau.alpha.tau.db));
return cljs.core.doall.call(null,(function (){var iter__4324__auto__ = ((function (ts){
return (function tau$alpha$tau$send_taus_$_iter__9294(s__9295){
return (new cljs.core.LazySeq(null,((function (ts){
return (function (){
var s__9295__$1 = s__9295;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__9295__$1);
if(temp__5457__auto__){
var s__9295__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__9295__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__9295__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__9297 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__9296 = (0);
while(true){
if((i__9296 < size__4323__auto__)){
var t = cljs.core._nth.call(null,c__4322__auto__,i__9296);
var s = tau.alpha.tau._get_sab.call(null,t);
var i = tau.alpha.tau._id.call(null,t);
cljs.core.chunk_append.call(null,b__9297,tau.alpha.tau.send_tau.call(null,port,s,i));

var G__9298 = (i__9296 + (1));
i__9296 = G__9298;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__9297),tau$alpha$tau$send_taus_$_iter__9294.call(null,cljs.core.chunk_rest.call(null,s__9295__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__9297),null);
}
} else {
var t = cljs.core.first.call(null,s__9295__$2);
var s = tau.alpha.tau._get_sab.call(null,t);
var i = tau.alpha.tau._id.call(null,t);
return cljs.core.cons.call(null,tau.alpha.tau.send_tau.call(null,port,s,i),tau$alpha$tau$send_taus_$_iter__9294.call(null,cljs.core.rest.call(null,s__9295__$2)));
}
} else {
return null;
}
break;
}
});})(ts))
,null,null));
});})(ts))
;
return iter__4324__auto__.call(null,ts);
})());
});
cljs.core.swap_BANG_.call(null,tau.alpha.state.new_port_fns,cljs.core.conj,tau.alpha.tau.send_taus);
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
cljs.core.swap_BANG_.call(null,tau.alpha.state.sabs,cljs.core.assoc,new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644),(new SharedArrayBuffer(((4) + ((((2) * (4)) * tau.alpha.tau._STAR_num_taus_STAR_) * tau.alpha.tau._STAR_tau_size_STAR_)))));

var s_9299 = (new Int32Array(new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs)),(0),(1)));
tau.alpha.tau.astore.call(null,s_9299,(0),(0));
} else {
}
tau.alpha.tau.reconstruct_tau = (function tau$alpha$tau$reconstruct_tau(s){
var tid = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(s);
var n1 = cljs.core.nth.call(null,cljs.core.name.call(null,tid),(2));
if(cljs.core._EQ_.call(null,n1,"_")){
return cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.tau.db),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join(''));
} else {
var n2 = cljs.core.nth.call(null,cljs.core.name.call(null,tid),(3));
var n = tau.alpha.util.read.call(null,((cljs.core._EQ_.call(null,n1,"0"))?n2:[cljs.core.str.cljs$core$IFn$_invoke$arity$1(n1),cljs.core.str.cljs$core$IFn$_invoke$arity$1(n2)].join('')));
var idx = tau.alpha.tau.get_next_ia_idx.call(null,n);
var sab = new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs));
var ia = (new Int32Array(sab,idx,((2) * tau.alpha.tau._STAR_tau_size_STAR_)));
var t = (new tau.alpha.tau.Tau(sab,ia,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join(''),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid)].join('')], null),null,null));
cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t)].join(''),t);

return t;
}
});
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"Tau","Tau",-743163819,null),(function (x){
var or__3949__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.tau.db),["#Tau ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(x)].join(''));
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return tau.alpha.tau.reconstruct_tau.call(null,x);
}
}));
tau.alpha.tau.exec_tau = (function tau$alpha$tau$exec_tau(var_args){
var G__9301 = arguments.length;
switch (G__9301) {
case 0:
return tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$0 = (function (){
return tau.alpha.tau.exec_tau.call(null,null);
});

tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$1 = (function (state){
return tau.alpha.tau.exec_tau.call(null,state,tau.alpha.tau._STAR_tau_size_STAR_);
});

tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$2 = (function (state,n){
return tau.alpha.tau.exec_tau.call(null,null,state,n);
});

tau.alpha.tau.exec_tau.cljs$core$IFn$_invoke$arity$3 = (function (tid,state,n){
var sab = new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs));
var n__$1 = tau.alpha.tau.get_next_ctr.call(null,sab);
var n__$2 = (((n__$1 < (10)))?["0",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n__$1)].join(''):[cljs.core.str.cljs$core$IFn$_invoke$arity$1(n__$1)].join(''));
var ia = (new Int32Array(sab,tau.alpha.tau.get_next_ia_idx.call(null,n__$2),((2) * tau.alpha.tau._STAR_tau_size_STAR_)));
var tid__$1 = [":tau/on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n__$2),"",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__3949__auto__ = tid;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return tau.alpha.util.get_new_id.call(null);
}
})())].join('');
var t = (new tau.alpha.tau.Tau(sab,ia,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid__$1)].join(''),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid__$1)].join('')], null),null,null));
cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t)].join(''),t);

tau.alpha.util.enc.call(null,ia,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"val","val",128701612),state,new cljs.core.Keyword(null,"watches","watches",-273097535),null,new cljs.core.Keyword(null,"validator","validator",-1966190681),null,new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),null], null));

return t;
});

tau.alpha.tau.exec_tau.cljs$lang$maxFixedArity = 3;

tau.alpha.tau.tau = (function tau$alpha$tau$tau(var_args){
var G__9306 = arguments.length;
switch (G__9306) {
case 0:
return tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
var args_arr__4546__auto__ = [];
var len__4531__auto___9310 = arguments.length;
var i__4532__auto___9311 = (0);
while(true){
if((i__4532__auto___9311 < len__4531__auto___9310)){
args_arr__4546__auto__.push((arguments[i__4532__auto___9311]));

var G__9312 = (i__4532__auto___9311 + (1));
i__4532__auto___9311 = G__9312;
continue;
} else {
}
break;
}

var argseq__4547__auto__ = (new cljs.core.IndexedSeq(args_arr__4546__auto__.slice((1)),(0),null));
return tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4547__auto__);

}
});

tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$0 = (function (){
return tau.alpha.tau.tau.call(null,null);
});

tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$1 = (function (state){
return tau.alpha.tau.tau.call(null,state,cljs.core.PersistentArrayMap.EMPTY);
});

tau.alpha.tau.tau.cljs$core$IFn$_invoke$arity$variadic = (function (state,options){
var map__9307 = cljs.core.apply.call(null,cljs.core.hash_map,options);
var map__9307__$1 = ((((!((map__9307 == null)))?(((((map__9307.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9307.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9307):map__9307);
var meta = cljs.core.get.call(null,map__9307__$1,new cljs.core.Keyword(null,"meta","meta",1499536964));
var executor = cljs.core.get.call(null,map__9307__$1,new cljs.core.Keyword(null,"executor","executor",1197215162));
var tid = cljs.core.get.call(null,map__9307__$1,new cljs.core.Keyword(null,"tid","tid",-901350880),tau.alpha.util.get_new_id.call(null));
var size = cljs.core.get.call(null,map__9307__$1,new cljs.core.Keyword(null,"size","size",1098693007),tau.alpha.tau._STAR_tau_size_STAR_);
var ab = cljs.core.get.call(null,map__9307__$1,new cljs.core.Keyword(null,"ab","ab",-839573926),false);
if(cljs.core.truth_(executor)){
return cljs.core.apply.call(null,tau.alpha.tau.exec_tau,tid,state,size);
} else {
var sab = (new SharedArrayBuffer((((2) * (4)) * size)));
var a = (new Int32Array(sab,(0),((2) * size)));
var tau_type = (cljs.core.truth_(ab)?"_":"*");
var tid__$1 = [":tau/on_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau_type),cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__3949__auto__ = tid;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return tau.alpha.util.get_new_id.call(null);
}
})())].join('');
var t = (new tau.alpha.tau.Tau(sab,a,tid__$1,cljs.core.merge.call(null,meta,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"id","id",-1388402092),tid__$1,new cljs.core.Keyword(null,"size","size",1098693007),size,new cljs.core.Keyword(null,"ab","ab",-839573926),ab], null)),null,null));
cljs.core.swap_BANG_.call(null,tau.alpha.tau.db,cljs.core.assoc,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t)].join(''),t);

if(cljs.core.truth_((function (){var or__3949__auto__ = cljs.core.array_QMARK_.call(null,state);
if(or__3949__auto__){
return or__3949__auto__;
} else {
return tau.alpha.util.typed_array_QMARK_.call(null,state);
}
})())){
tau.alpha.tau.enc_ab.call(null,a,state);
} else {
tau.alpha.util.enc.call(null,a,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"val","val",128701612),state,new cljs.core.Keyword(null,"watches","watches",-273097535),null,new cljs.core.Keyword(null,"validator","validator",-1966190681),null,new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),null], null));
}

tau.alpha.tau.dist_tau.call(null,t);

return t;
}
});

/** @this {Function} */
tau.alpha.tau.tau.cljs$lang$applyTo = (function (seq9304){
var G__9305 = cljs.core.first.call(null,seq9304);
var seq9304__$1 = cljs.core.next.call(null,seq9304);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9305,seq9304__$1);
});

tau.alpha.tau.tau.cljs$lang$maxFixedArity = (1);


//# sourceMappingURL=tau.js.map
