// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.on');
goog.require('cljs.core');
goog.require('tau.alpha.io');
goog.require('tau.alpha.call');
goog.require('tau.alpha.util');
goog.require('tau.alpha.tau');
goog.require('tau.alpha.state');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.on.run_launch = (function tau$alpha$on$run_launch(){
tau.alpha.io.connect.call(null);

cljs.core.swap_BANG_.call(null,tau.alpha.state.loaded_QMARK_,cljs.core.constantly.call(null,true));

var temp__5455__auto___9315 = tau.alpha.util.read.call(null,(taupreload["args"]));
if(cljs.core.truth_(temp__5455__auto___9315)){
var v_9316 = temp__5455__auto___9315;
tau.alpha.on.init_val = v_9316;
} else {
}

var temp__5455__auto___9317 = tau.alpha.util.read.call(null,(taupreload["fn"]));
if(cljs.core.truth_(temp__5455__auto___9317)){
var afn_9318 = temp__5455__auto___9317;
tau.alpha.on.init_fn = afn_9318;
} else {
}

if(cljs.core.truth_((function (){var and__3938__auto__ = tau.alpha.on.init_fn;
if(cljs.core.truth_(and__3938__auto__)){
return tau.alpha.on.init_val;
} else {
return and__3938__auto__;
}
})())){
if(cljs.core.seqable_QMARK_.call(null,tau.alpha.on.init_val)){
return cljs.core.apply.call(null,tau.alpha.call.call,tau.alpha.on.init_fn,tau.alpha.on.init_val);
} else {
return tau.alpha.call.call.call(null,tau.alpha.on.init_fn,tau.alpha.on.init_val);
}
} else {
return null;
}
});
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
tau.alpha.on.exec_db = tau.alpha.tau.exec_tau.call(null,cljs.core.PersistentArrayMap.EMPTY);
} else {
}
tau.alpha.on.ui_tauon = (function tau$alpha$on$ui_tauon(var_args){
var G__9320 = arguments.length;
switch (G__9320) {
case 1:
return tau.alpha.on.ui_tauon.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 3:
return tau.alpha.on.ui_tauon.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.on.ui_tauon.cljs$core$IFn$_invoke$arity$1 = (function (value){
return tau.alpha.on.ui_tauon.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [value], null),null,null);
});

tau.alpha.on.ui_tauon.cljs$core$IFn$_invoke$arity$3 = (function (value,afn,tid){
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
var tid__$1 = (cljs.core.truth_(tid)?tid:["f_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.util.new_id.call(null))].join(''));
var tid__$2 = ((typeof tid__$1 === 'string')?tau.alpha.util.read.call(null,tid__$1):tid__$1);
var w = tau.alpha.io.worker.call(null,tau.alpha.util.get_worker_script.call(null,null,new cljs.core.Keyword(null,"main","main",-2117802661).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.conf)),tau.alpha.util.write.call(null,afn),value,tid__$2),cljs.core.deref.call(null,tau.alpha.state.serve_handlers));
var main_pool = new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs));
var int32 = tau.alpha.tau._get_int32a.call(null,tau.alpha.on.exec_db);
var t = tau.alpha.tau._id.call(null,tau.alpha.on.exec_db);
cljs.core.swap_BANG_.call(null,tau.alpha.state.ports,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tid__$2,new cljs.core.Keyword(null,"w","w",354169001)], null),w);

tau.alpha.io.dist_ports.call(null,tid__$2);

tau.alpha.io.do_on.call(null,tid__$2,((function (tid__$1,tid__$2,w,main_pool,int32,t){
return (function (main_pool__$1){
return cljs.core.swap_BANG_.call(null,tau.alpha.state.sabs,cljs.core.assoc,new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644),main_pool__$1);
});})(tid__$1,tid__$2,w,main_pool,int32,t))
,null,main_pool);

tau.alpha.io.do_on.call(null,tid__$2,((function (tid__$1,tid__$2,w,main_pool,int32,t){
return (function (){
return tau.alpha.on.run_launch.call(null);
});})(tid__$1,tid__$2,w,main_pool,int32,t))
,cljs.core.PersistentArrayMap.EMPTY);

tau.alpha.io.do_on.call(null,tid__$2,((function (tid__$1,tid__$2,w,main_pool,int32,t){
return (function (int32__$1,t__$1){
return (
tau.alpha.on.exec_db = (new tau.alpha.tau.Tau(new cljs.core.Keyword(null,"main-pool","main-pool",-2087883644).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.sabs)),int32__$1,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t__$1)].join(''),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(t__$1)].join('')], null),null,null)))
;
});})(tid__$1,tid__$2,w,main_pool,int32,t))
,null,int32,t);

return tid__$2;
} else {
return null;
}
});

tau.alpha.on.ui_tauon.cljs$lang$maxFixedArity = 3;

tau.alpha.on.new_id_key = (function tau$alpha$on$new_id_key(){
var nid = tau.alpha.util.get_new_id.call(null);
var c = cljs.core.count.call(null,nid);
if((c < (32))){
return cljs.core.keyword.call(null,nid);
} else {
return nid;
}
});
tau.alpha.on.tauon = (function tau$alpha$on$tauon(var_args){
var G__9323 = arguments.length;
switch (G__9323) {
case 0:
return tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$0 = (function (){
return tau.alpha.on.tauon.call(null,tau.alpha.on.new_id_key.call(null),null,null);
});

tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$1 = (function (v){
return tau.alpha.on.tauon.call(null,tau.alpha.on.new_id_key.call(null),null,v);
});

tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$2 = (function (afn,v){
return tau.alpha.on.tauon.call(null,tau.alpha.on.new_id_key.call(null),afn,v);
});

tau.alpha.on.tauon.cljs$core$IFn$_invoke$arity$3 = (function (tid,afn,v){
var tid__$1 = (cljs.core.truth_(tid)?tid:tau.alpha.on.new_id_key.call(null));
var tid__$2 = ((typeof tid__$1 === 'string')?tau.alpha.util.read.call(null,tid__$1):tid__$1);
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
tau.alpha.on.ui_tauon.call(null,v,afn,tid__$2);
} else {
if(cljs.core.truth_(cljs.core.deref.call(null,tau.alpha.state.loaded_QMARK_))){
tau.alpha.io.do_on.call(null,tau.alpha.state.screen,((function (tid__$1,tid__$2){
return (function (afn__$1,v__$1,tid__$3){
return tau.alpha.on.ui_tauon.call(null,v__$1,afn__$1,tid__$3);
});})(tid__$1,tid__$2))
,null,afn,v,tid__$2);
} else {
}
}

return tid__$2;
});

tau.alpha.on.tauon.cljs$lang$maxFixedArity = 3;

tau.alpha.on.kill = (function tau$alpha$on$kill(t){
return tau.alpha.io.do_on.call(null,t,(function (){
tau.alpha.io.plog.call(null,"tau.alpha.on",73,null,"tauon killed");

return tau.alpha.io.kill_local.call(null);
}),cljs.core.PersistentArrayMap.EMPTY);
});
tau.alpha.state.add_listeners.call(null,cljs.core.deref.call(null,tau.alpha.state.serve_handlers));

//# sourceMappingURL=on.js.map
