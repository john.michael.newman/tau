// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.example.fps');
goog.require('cljs.core');
goog.require('tau.alpha.example.utils');
tau.alpha.example.fps.fps_mon = (function tau$alpha$example$fps$fps_mon(times,fps){
return tau.alpha.example.utils.raf.call(null,(function (){
var now = performance.now();
while(true){
if((((cljs.core.deref.call(null,times).length > (0))) && (((cljs.core.deref.call(null,times)[(0)]) <= (now - (1000)))))){
cljs.core.swap_BANG_.call(null,times,((function (now){
return (function (p1__9331_SHARP_){
p1__9331_SHARP_.shift();

return p1__9331_SHARP_;
});})(now))
);

continue;
} else {
}
break;
}

cljs.core.swap_BANG_.call(null,times,((function (now){
return (function (p1__9332_SHARP_){
p1__9332_SHARP_.push(now);

return p1__9332_SHARP_;
});})(now))
);

cljs.core.reset_BANG_.call(null,fps,cljs.core.deref.call(null,times).length);

return tau.alpha.example.fps.fps_mon.call(null,times,fps);
}));
});
tau.alpha.example.fps.fps_atom = (function tau$alpha$example$fps$fps_atom(){
if(cljs.core.truth_(tau.alpha.example.utils.raf)){
var times = cljs.core.atom.call(null,[]);
var fps = cljs.core.atom.call(null,(0));
tau.alpha.example.fps.fps_mon.call(null,times,fps);

return fps;
} else {
return null;
}
});
tau.alpha.example.fps.fps = tau.alpha.example.fps.fps_atom.call(null);

//# sourceMappingURL=fps.js.map
