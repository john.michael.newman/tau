// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.exec');
goog.require('cljs.core');
goog.require('tau.alpha.state');
goog.require('tau.alpha.util');
goog.require('tau.alpha.call');
goog.require('tau.alpha.tau');
goog.require('tau.alpha.on');
goog.require('cljs.pprint');
goog.require('clojure.walk');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.exec.num_cores = (function tau$alpha$exec$num_cores(){
return self.navigator.hardwareConcurrency;
});
tau.alpha.exec.mk_pool = (function tau$alpha$exec$mk_pool(pid){
cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.assoc,pid,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"val","val",128701612),null,new cljs.core.Keyword(null,"ready","ready",1086465795),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"pending","pending",-220036727),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"pq","pq",2125521911),cljs.core.into.call(null,cljs.core.PersistentQueue.EMPTY,cljs.core.PersistentVector.EMPTY)], null));

return pid;
});
tau.alpha.exec.queue = (function tau$alpha$exec$queue(form){
return cljs.core.into.call(null,cljs.core.PersistentQueue.EMPTY,form);
});
tau.alpha.exec.popq_BANG_ = (function tau$alpha$exec$popq_BANG_(pid){
var q = tau.alpha.exec.queue.call(null,cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pq","pq",2125521911)], null)));
var m = cljs.core.peek.call(null,q);
cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pq","pq",2125521911)], null),((function (q,m){
return (function (){
return cljs.core.pop.call(null,q);
});})(q,m))
);

return m;
});
tau.alpha.exec.next_tauon = (function tau$alpha$exec$next_tauon(pid){
var i = cljs.core.ffirst.call(null,cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"ready","ready",1086465795)], null)));
var _ = cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"ready","ready",1086465795)], null),cljs.core.dissoc,i);
var ___$1 = cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pending","pending",-220036727)], null),cljs.core.assoc,i,i);
return i;
});
tau.alpha.exec.unpend = (function tau$alpha$exec$unpend(pid,k){
if(!((k == null))){
} else {
throw (new Error("Assert failed: (not (nil? k))"));
}

if(cljs.core.truth_(cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pending","pending",-220036727),k], null)))){
} else {
throw (new Error("Assert failed: (get-in (clojure.core/deref exec-db) [pid :pending k])"));
}

var i = cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pending","pending",-220036727),k], null));
var _ = cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pending","pending",-220036727)], null),cljs.core.dissoc,i);
var ___$1 = cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"ready","ready",1086465795)], null),cljs.core.assoc,i,i);
return i;
});
tau.alpha.exec.mk_fixed_thread_pool = (function tau$alpha$exec$mk_fixed_thread_pool(pid,n){
tau.alpha.io.do_on.call(null,"screen",(function (pid__$1,n__$1){
var pid__$2 = tau.alpha.exec.mk_pool.call(null,pid__$1);
var pts = cljs.core.doall.call(null,cljs.core.map.call(null,((function (pid__$2){
return (function (){
return tau.alpha.on.tauon.call(null,(1));
});})(pid__$2))
,cljs.core.range.call(null,n__$1)));
return cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid__$2,new cljs.core.Keyword(null,"ready","ready",1086465795)], null),cljs.core.zipmap.call(null,pts,pts));
}),null,pid,n);

return pid;
});
tau.alpha.exec.enqueue = (function tau$alpha$exec$enqueue(pid,m){
return cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pq","pq",2125521911)], null),cljs.core.comp.call(null,tau.alpha.exec.queue,cljs.core.conj),m);
});
tau.alpha.exec.tau_afn = cljs.core.atom.call(null,null);
tau.alpha.exec.tau_tau = cljs.core.atom.call(null,null);
tau.alpha.exec.submit_action = (function tau$alpha$exec$submit_action(pid,local_id,atauon,atau,conveyer){
tau.alpha.io.do_on.call(null,atauon,(function (atau__$1,pid__$1,local_id__$1,conveyer__$1){
var afn = new cljs.core.Keyword(null,"afn","afn",-1423568060).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,atau__$1));
var _ = cljs.core.reset_BANG_.call(null,tau.alpha.exec.tau_tau,atau__$1);
var ___$1 = cljs.core.reset_BANG_.call(null,tau.alpha.exec.tau_afn,afn);
var conveyed = tau.alpha.call.respile_args.call(null,conveyer__$1);
var res = (cljs.core.truth_(afn)?(function (){try{return cljs.core.apply.call(null,tau.alpha.call.call,afn,conveyed);
}catch (e9363){var e = e9363;
return cljs.core.println.call(null,"error:",e);
}})():null);
return res;
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),(function (p1__9362_SHARP_){
return cljs.core.println.call(null,p1__9362_SHARP_);
})], null),atau,pid,local_id,conveyer);

tau.alpha.exec.unpend.call(null,pid,atauon);

var x9364 = atau;
x9364.cljs$core$IDeref$ = cljs.core.PROTOCOL_SENTINEL;

x9364.cljs$core$IDeref$_deref$arity$1 = ((function (x9364){
return (function (this$){
var this$__$1 = this;
var map__9365 = new cljs.core.Keyword(null,"val","val",128701612).cljs$core$IFn$_invoke$arity$1(tau.alpha.util.unc.call(null,tau.alpha.tau._get_int32a.call(null,atau)));
var map__9365__$1 = ((((!((map__9365 == null)))?(((((map__9365.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9365.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9365):map__9365);
var completed_QMARK_ = cljs.core.get.call(null,map__9365__$1,new cljs.core.Keyword(null,"completed?","completed?",946828354));
var result = cljs.core.get.call(null,map__9365__$1,new cljs.core.Keyword(null,"result","result",1415092211));
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
return result;
} else {
if(cljs.core.truth_(completed_QMARK_)){
return result;
} else {
return new cljs.core.Keyword(null,"result","result",1415092211).cljs$core$IFn$_invoke$arity$1(tau.alpha.tau.wait.call(null,atau));
}
}
});})(x9364))
;

return x9364;
});
tau.alpha.exec.run_q = (function tau$alpha$exec$run_q(pid){
var temp__5455__auto__ = cljs.core.peek.call(null,tau.alpha.exec.queue.call(null,cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [pid,new cljs.core.Keyword(null,"pq","pq",2125521911)], null))));
if(cljs.core.truth_(temp__5455__auto__)){
var map__9367 = temp__5455__auto__;
var map__9367__$1 = ((((!((map__9367 == null)))?(((((map__9367.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9367.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9367):map__9367);
var atau = cljs.core.get.call(null,map__9367__$1,new cljs.core.Keyword(null,"atau","atau",-1229839438));
var afn = cljs.core.get.call(null,map__9367__$1,new cljs.core.Keyword(null,"afn","afn",-1423568060));
var conveyer = cljs.core.get.call(null,map__9367__$1,new cljs.core.Keyword(null,"conveyer","conveyer",1540205575));
var temp__5455__auto____$1 = tau.alpha.exec.next_tauon.call(null,pid);
if(cljs.core.truth_(temp__5455__auto____$1)){
var t = temp__5455__auto____$1;
var _ = tau.alpha.exec.popq_BANG_.call(null,pid);
var local_id = (((tau.alpha.state.id instanceof cljs.core.Keyword))?tau.alpha.state.id:(cljs.core.truth_(tau.alpha.state.id.startsWith(":"))?tau.alpha.util.read.call(null,tau.alpha.state.id):cljs.core.keyword.call(null,tau.alpha.state.id)));
var local_id__$1 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.state.id)].join('');
var pid__$1 = (((pid instanceof tau.alpha.exec.Executor))?new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(cljs.core.meta.call(null,pid)):pid);
return tau.alpha.exec.submit_action.call(null,pid__$1,local_id__$1,t,atau,conveyer);
} else {
return null;
}
} else {
return null;
}
});
tau.alpha.exec.local_submit = (function tau$alpha$exec$local_submit(pid,afn,conveyer){
var pid__$1 = (((pid instanceof tau.alpha.exec.Executor))?new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(cljs.core.meta.call(null,pid)):pid);
var atau = tau.alpha.tau.exec_tau.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"completed?","completed?",946828354),false,new cljs.core.Keyword(null,"afn","afn",-1423568060),cljs.core.pr_str.call(null,afn),new cljs.core.Keyword(null,"result","result",1415092211),null], null));
tau.alpha.exec.enqueue.call(null,pid__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"atau","atau",-1229839438),atau,new cljs.core.Keyword(null,"afn","afn",-1423568060),afn,new cljs.core.Keyword(null,"conveyer","conveyer",1540205575),conveyer], null));

tau.alpha.util.sleep.call(null,(20));

var res = tau.alpha.exec.run_q.call(null,pid__$1);
return res;
});
tau.alpha.exec.mk_executor = (function tau$alpha$exec$mk_executor(oid,state,opts){
var thread_pool = tau.alpha.exec.mk_fixed_thread_pool.call(null,oid,new cljs.core.Keyword(null,"num-threads","num-threads",-157240048).cljs$core$IFn$_invoke$arity$1(opts));
cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [thread_pool,new cljs.core.Keyword(null,"val","val",128701612)], null),state);

cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update,oid,cljs.core.merge,opts);

return thread_pool;
});
tau.alpha.exec.time_trip = (function tau$alpha$exec$time_trip(f){
var t = (new Date()).getTime();
return tau.alpha.io.do_on.call(null,f,((function (t){
return (function (t__$1){
return tau.alpha.io.plog.call(null,"tau.alpha.exec",113,null,((new Date()).getTime() - t__$1));
});})(t))
,null,t);
});

/**
 * Marker protocol indicating an atom shared between web workers.
 * @interface
 */
tau.alpha.exec.IExecutor = function(){};

/**
 * Returns the number of threads in the executor thread pool.
 */
tau.alpha.exec._num_threads = (function tau$alpha$exec$_num_threads(n){
if(((!((n == null))) && (!((n.tau$alpha$exec$IExecutor$_num_threads$arity$1 == null))))){
return n.tau$alpha$exec$IExecutor$_num_threads$arity$1(n);
} else {
var x__4243__auto__ = (((n == null))?null:n);
var m__4244__auto__ = (tau.alpha.exec._num_threads[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,n);
} else {
var m__4244__auto____$1 = (tau.alpha.exec._num_threads["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,n);
} else {
throw cljs.core.missing_protocol.call(null,"IExecutor.-num-threads",n);
}
}
}
});


/**
 * Protocol for adding for adding functionality for getting the ID of an object.
 * @interface
 */
tau.alpha.exec.IDable = function(){};

tau.alpha.exec._id = (function tau$alpha$exec$_id(o){
if(((!((o == null))) && (!((o.tau$alpha$exec$IDable$_id$arity$1 == null))))){
return o.tau$alpha$exec$IDable$_id$arity$1(o);
} else {
var x__4243__auto__ = (((o == null))?null:o);
var m__4244__auto__ = (tau.alpha.exec._id[goog.typeOf(x__4243__auto__)]);
if(!((m__4244__auto__ == null))){
return m__4244__auto__.call(null,o);
} else {
var m__4244__auto____$1 = (tau.alpha.exec._id["_"]);
if(!((m__4244__auto____$1 == null))){
return m__4244__auto____$1.call(null,o);
} else {
throw cljs.core.missing_protocol.call(null,"IDable.-id",o);
}
}
}
});

tau.alpha.exec.oid = (function tau$alpha$exec$oid(a){
if((a instanceof cljs.core.Keyword)){
return a;
} else {
return tau.alpha.exec._id.call(null,a);
}
});

/**
* @constructor
 * @implements {cljs.core.IWatchable}
 * @implements {tau.alpha.exec.IExecutor}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.IMeta}
 * @implements {tau.alpha.exec.IDable}
 * @implements {cljs.core.IDeref}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {tau.alpha.exec.Object}
*/
tau.alpha.exec.Executor = (function (state,oid,meta,validator,watches,num_threads){
this.state = state;
this.oid = oid;
this.meta = meta;
this.validator = validator;
this.watches = watches;
this.num_threads = num_threads;
this.cljs$lang$protocol_mask$partition0$ = 2153938944;
this.cljs$lang$protocol_mask$partition1$ = 2;
});
tau.alpha.exec.Executor.prototype.equiv = (function (other){
var self__ = this;
var this$ = this;
return cljs.core._equiv.call(null,this$,other);
});

tau.alpha.exec.Executor.prototype.toString = (function (){
var self__ = this;
var _ = this;
return ["#Executor ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta))].join('');
});

tau.alpha.exec.Executor.prototype.tau$alpha$exec$IExecutor$ = cljs.core.PROTOCOL_SENTINEL;

tau.alpha.exec.Executor.prototype.tau$alpha$exec$IExecutor$_num_threads$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta),new cljs.core.Keyword(null,"meta","meta",1499536964)], null));
});

tau.alpha.exec.Executor.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (o,writer,opts){
var self__ = this;
var o__$1 = this;
return cljs.core._write.call(null,writer,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(o__$1)].join(''));
});

tau.alpha.exec.Executor.prototype.tau$alpha$exec$IDable$ = cljs.core.PROTOCOL_SENTINEL;

tau.alpha.exec.Executor.prototype.tau$alpha$exec$IDable$_id$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta);
});

tau.alpha.exec.Executor.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (o,other){
var self__ = this;
var o__$1 = this;
return (o__$1 === other);
});

tau.alpha.exec.Executor.prototype.cljs$core$IDeref$_deref$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta),new cljs.core.Keyword(null,"val","val",128701612)], null));
});

tau.alpha.exec.Executor.prototype.cljs$core$IMeta$_meta$arity$1 = (function (o){
var self__ = this;
var o__$1 = this;
return cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta),new cljs.core.Keyword(null,"meta","meta",1499536964)], null));
});

tau.alpha.exec.Executor.prototype.cljs$core$IWatchable$_notify_watches$arity$3 = (function (o,old_val,new_val){
var self__ = this;
var o__$1 = this;
var temp__5455__auto__ = cljs.core.get_in.call(null,cljs.core.deref.call(null,tau.alpha.on.exec_db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(self__.meta),new cljs.core.Keyword(null,"watches","watches",-273097535)], null));
if(cljs.core.truth_(temp__5455__auto__)){
var watches__$1 = temp__5455__auto__;
var seq__9369 = cljs.core.seq.call(null,watches__$1);
var chunk__9370 = null;
var count__9371 = (0);
var i__9372 = (0);
while(true){
if((i__9372 < count__9371)){
var vec__9373 = cljs.core._nth.call(null,chunk__9370,i__9372);
var key = cljs.core.nth.call(null,vec__9373,(0),null);
var f = cljs.core.nth.call(null,vec__9373,(1),null);
f.call(null,key,o__$1,old_val,new_val);


var G__9379 = seq__9369;
var G__9380 = chunk__9370;
var G__9381 = count__9371;
var G__9382 = (i__9372 + (1));
seq__9369 = G__9379;
chunk__9370 = G__9380;
count__9371 = G__9381;
i__9372 = G__9382;
continue;
} else {
var temp__5457__auto__ = cljs.core.seq.call(null,seq__9369);
if(temp__5457__auto__){
var seq__9369__$1 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__9369__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__9369__$1);
var G__9383 = cljs.core.chunk_rest.call(null,seq__9369__$1);
var G__9384 = c__4351__auto__;
var G__9385 = cljs.core.count.call(null,c__4351__auto__);
var G__9386 = (0);
seq__9369 = G__9383;
chunk__9370 = G__9384;
count__9371 = G__9385;
i__9372 = G__9386;
continue;
} else {
var vec__9376 = cljs.core.first.call(null,seq__9369__$1);
var key = cljs.core.nth.call(null,vec__9376,(0),null);
var f = cljs.core.nth.call(null,vec__9376,(1),null);
f.call(null,key,o__$1,old_val,new_val);


var G__9387 = cljs.core.next.call(null,seq__9369__$1);
var G__9388 = null;
var G__9389 = (0);
var G__9390 = (0);
seq__9369 = G__9387;
chunk__9370 = G__9388;
count__9371 = G__9389;
i__9372 = G__9390;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});

tau.alpha.exec.Executor.prototype.cljs$core$IWatchable$_add_watch$arity$3 = (function (o,key,f){
var self__ = this;
var o__$1 = this;
cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [o__$1,new cljs.core.Keyword(null,"watches","watches",-273097535),key], null),f);

return o__$1;
});

tau.alpha.exec.Executor.prototype.cljs$core$IWatchable$_remove_watch$arity$2 = (function (o,key){
var self__ = this;
var o__$1 = this;
return cljs.core.swap_BANG_.call(null,tau.alpha.on.exec_db,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [o__$1,new cljs.core.Keyword(null,"watches","watches",-273097535)], null),cljs.core.dissoc,key);
});

tau.alpha.exec.Executor.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return goog.getUid(this$__$1);
});

tau.alpha.exec.Executor.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"state","state",-348086572,null),new cljs.core.Symbol(null,"oid","oid",871839193,null),new cljs.core.Symbol(null,"meta","meta",-1154898805,null),new cljs.core.Symbol(null,"validator","validator",-325659154,null),new cljs.core.Symbol(null,"watches","watches",1367433992,null),new cljs.core.Symbol(null,"num-threads","num-threads",1483291479,null)], null);
});

tau.alpha.exec.Executor.cljs$lang$type = true;

tau.alpha.exec.Executor.cljs$lang$ctorStr = "tau.alpha.exec/Executor";

tau.alpha.exec.Executor.cljs$lang$ctorPrWriter = (function (this__4192__auto__,writer__4193__auto__,opt__4194__auto__){
return cljs.core._write.call(null,writer__4193__auto__,"tau.alpha.exec/Executor");
});

/**
 * Positional factory function for tau.alpha.exec/Executor.
 */
tau.alpha.exec.__GT_Executor = (function tau$alpha$exec$__GT_Executor(state,oid,meta,validator,watches,num_threads){
return (new tau.alpha.exec.Executor(state,oid,meta,validator,watches,num_threads));
});

tau.alpha.exec.new_id_key = (function tau$alpha$exec$new_id_key(){
return cljs.core.keyword.call(null,"tau.alpha.exec",tau.alpha.util.get_new_id.call(null));
});
tau.alpha.exec.num_threads = (function tau$alpha$exec$num_threads(o){
return tau.alpha.exec._num_threads.call(null,o);
});
tau.alpha.exec.executor_object_facade = (function tau$alpha$exec$executor_object_facade(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9396 = arguments.length;
var i__4532__auto___9397 = (0);
while(true){
if((i__4532__auto___9397 < len__4531__auto___9396)){
args__4534__auto__.push((arguments[i__4532__auto___9397]));

var G__9398 = (i__4532__auto___9397 + (1));
i__4532__auto___9397 = G__9398;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return tau.alpha.exec.executor_object_facade.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

tau.alpha.exec.executor_object_facade.cljs$core$IFn$_invoke$arity$variadic = (function (x,p__9393){
var map__9394 = p__9393;
var map__9394__$1 = ((((!((map__9394 == null)))?(((((map__9394.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9394.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9394):map__9394);
var meta = cljs.core.get.call(null,map__9394__$1,new cljs.core.Keyword(null,"meta","meta",1499536964));
var validator = cljs.core.get.call(null,map__9394__$1,new cljs.core.Keyword(null,"validator","validator",-1966190681));
var num_threads = cljs.core.get.call(null,map__9394__$1,new cljs.core.Keyword(null,"num-threads","num-threads",-157240048));
return (new tau.alpha.exec.Executor(x,new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(meta),meta,validator,null,num_threads));
});

tau.alpha.exec.executor_object_facade.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
tau.alpha.exec.executor_object_facade.cljs$lang$applyTo = (function (seq9391){
var G__9392 = cljs.core.first.call(null,seq9391);
var seq9391__$1 = cljs.core.next.call(null,seq9391);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9392,seq9391__$1);
});

tau.alpha.exec.exec = (function tau$alpha$exec$exec(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9401 = arguments.length;
var i__4532__auto___9402 = (0);
while(true){
if((i__4532__auto___9402 < len__4531__auto___9401)){
args__4534__auto__.push((arguments[i__4532__auto___9402]));

var G__9403 = (i__4532__auto___9402 + (1));
i__4532__auto___9402 = G__9403;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return tau.alpha.exec.exec.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

tau.alpha.exec.exec.cljs$core$IFn$_invoke$arity$variadic = (function (state,options){
var opts = cljs.core.apply.call(null,cljs.core.hash_map,options);
var num_threads = (function (){var temp__5455__auto__ = new cljs.core.Keyword(null,"num-threads","num-threads",-157240048).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(temp__5455__auto__)){
var n = temp__5455__auto__;
return n;
} else {
return ((1) + tau.alpha.exec.num_cores.call(null));
}
})();
var oid = (function (){var temp__5455__auto__ = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(opts));
if(cljs.core.truth_(temp__5455__auto__)){
var i = temp__5455__auto__;
return i;
} else {
return tau.alpha.exec.new_id_key.call(null);
}
})();
var meta = (function (){var temp__5455__auto__ = new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(temp__5455__auto__)){
var m = temp__5455__auto__;
return m;
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),oid], null);
}
})();
var opts__$1 = cljs.core.merge.call(null,opts,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"meta","meta",1499536964),meta,new cljs.core.Keyword(null,"num-threads","num-threads",-157240048),num_threads], null));
var o = tau.alpha.exec.executor_object_facade.call(null,state,opts__$1);
var temp__5455__auto___9404 = new cljs.core.Keyword(null,"meta","meta",1499536964).cljs$core$IFn$_invoke$arity$1(opts__$1);
if(cljs.core.truth_(temp__5455__auto___9404)){
var m_9405 = temp__5455__auto___9404;
o.meta = m_9405;
} else {
}

var temp__5455__auto___9406 = new cljs.core.Keyword(null,"num-threads","num-threads",-157240048).cljs$core$IFn$_invoke$arity$1(opts__$1);
if(cljs.core.truth_(temp__5455__auto___9406)){
var t_9407 = temp__5455__auto___9406;
o.num_threads = t_9407;
} else {
}

tau.alpha.exec.mk_executor.call(null,oid,state,opts__$1);

return o;
});

tau.alpha.exec.exec.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
tau.alpha.exec.exec.cljs$lang$applyTo = (function (seq9399){
var G__9400 = cljs.core.first.call(null,seq9399);
var seq9399__$1 = cljs.core.next.call(null,seq9399);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9400,seq9399__$1);
});

tau.alpha.exec.init_default_executor = (function tau$alpha$exec$init_default_executor(){
if(cljs.core.not.call(null,new cljs.core.Keyword(null,"default-executor","default-executor",-333011988).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.on.exec_db)))){
return setTimeout((function (){
return tau.alpha.exec.exec.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"meta","meta",1499536964),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"default-executor","default-executor",-333011988)], null));
}),(100));
} else {
return null;
}
});
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
} else {
}

//# sourceMappingURL=exec.js.map
