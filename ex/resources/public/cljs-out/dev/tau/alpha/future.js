// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.future');
goog.require('cljs.core');
goog.require('tau.alpha.exec');
tau.alpha.future.yield$ = (function tau$alpha$future$yield(arg){
if(cljs.core.truth_((function (){var and__3938__auto__ = cljs.core.deref.call(null,tau.alpha.exec.tau_tau);
if(cljs.core.truth_(and__3938__auto__)){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"yielded?","yielded?",-2036983499).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,cljs.core.deref.call(null,tau.alpha.exec.tau_tau))));
} else {
return and__3938__auto__;
}
})())){
return cljs.core.swap_BANG_.call(null,cljs.core.deref.call(null,tau.alpha.exec.tau_tau),(function (_){
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"yielded?","yielded?",-2036983499),true,new cljs.core.Keyword(null,"completed?","completed?",946828354),true,new cljs.core.Keyword(null,"result","result",1415092211),arg], null);
}));
} else {
return null;
}
});
tau.alpha.future.future_call = (function tau$alpha$future$future_call(var_args){
var G__9594 = arguments.length;
switch (G__9594) {
case 1:
return tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$1 = (function (f){
return tau.alpha.future.future_call.call(null,new cljs.core.Keyword(null,"default-executor","default-executor",-333011988),cljs.core.PersistentVector.EMPTY,f);
});

tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$2 = (function (conveyer,f){
return tau.alpha.future.future_call.call(null,new cljs.core.Keyword(null,"default-executor","default-executor",-333011988),conveyer,f);
});

tau.alpha.future.future_call.cljs$core$IFn$_invoke$arity$3 = (function (ex,conveyer,f){
return tau.alpha.exec.local_submit.call(null,((cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"default-executor","default-executor",-333011988),ex))?ex:tau.alpha.exec.oid.call(null,ex)),f,conveyer);
});

tau.alpha.future.future_call.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=future.js.map
