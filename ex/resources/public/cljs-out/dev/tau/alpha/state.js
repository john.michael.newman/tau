// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.state');
goog.require('cljs.core');
goog.require('cljs.reader');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.state.start_time = (new Date()).getTime();
tau.alpha.state.conf = cljs.core.atom.call(null,null);
tau.alpha.state.set_conf_BANG_ = (function tau$alpha$state$set_conf_BANG_(conf_map){
return cljs.core.swap_BANG_.call(null,tau.alpha.state.conf,cljs.core.constantly.call(null,conf_map));
});
tau.alpha.state.off_screen_QMARK_ = (function tau$alpha$state$off_screen_QMARK_(){
return (void 0 === self.document);
});
tau.alpha.state.on_screen_QMARK_ = cljs.core.complement.call(null,tau.alpha.state.off_screen_QMARK_);
tau.alpha.state.ports = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
tau.alpha.state.sabs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
tau.alpha.state.default_executor = cljs.core.atom.call(null,null);
tau.alpha.state.loaded_QMARK_ = cljs.core.atom.call(null,false);
tau.alpha.state.repl_fn = (function tau$alpha$state$repl_fn(var_args){
var args__4534__auto__ = [];
var len__4531__auto___7999 = arguments.length;
var i__4532__auto___8000 = (0);
while(true){
if((i__4532__auto___8000 < len__4531__auto___7999)){
args__4534__auto__.push((arguments[i__4532__auto___8000]));

var G__8001 = (i__4532__auto___8000 + (1));
i__4532__auto___8000 = G__8001;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((0) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((0)),(0),null)):null);
return tau.alpha.state.repl_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__4535__auto__);
});

tau.alpha.state.repl_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return null;
});

tau.alpha.state.repl_fn.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
tau.alpha.state.repl_fn.cljs$lang$applyTo = (function (seq7998){
var self__4519__auto__ = this;
return self__4519__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq7998));
});

tau.alpha.state.screen = "screen";
tau.alpha.state.get_id = (function tau$alpha$state$get_id(){
if((typeof taupreload !== 'undefined')){
return taupreload.tid;
} else {
return "screen";
}
});
tau.alpha.state.id = tau.alpha.state.get_id.call(null);
tau.alpha.state.serve_handlers = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
tau.alpha.state.add_handler = (function tau$alpha$state$add_handler(h){
return cljs.core.swap_BANG_.call(null,tau.alpha.state.serve_handlers,cljs.core.merge,h);
});
tau.alpha.state.remove_handler = (function tau$alpha$state$remove_handler(k){
return cljs.core.swap_BANG_.call(null,tau.alpha.state.serve_handlers,cljs.core.dissoc,k);
});
tau.alpha.state.event_message = "message";
tau.alpha.state.deserialize = (function tau$alpha$state$deserialize(data){
return cljs.reader.read_string.call(null,data);
});
tau.alpha.state.serialize = (function tau$alpha$state$serialize(data){
return cljs.core.pr_str.call(null,data);
});
tau.alpha.state.map_key = (function tau$alpha$state$map_key(f,m){
return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4324__auto__ = (function tau$alpha$state$map_key_$_iter__8002(s__8003){
return (new cljs.core.LazySeq(null,(function (){
var s__8003__$1 = s__8003;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__8003__$1);
if(temp__5457__auto__){
var s__8003__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__8003__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__8003__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__8005 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__8004 = (0);
while(true){
if((i__8004 < size__4323__auto__)){
var vec__8006 = cljs.core._nth.call(null,c__4322__auto__,i__8004);
var k = cljs.core.nth.call(null,vec__8006,(0),null);
var v = cljs.core.nth.call(null,vec__8006,(1),null);
cljs.core.chunk_append.call(null,b__8005,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [f.call(null,k),v], null));

var G__8012 = (i__8004 + (1));
i__8004 = G__8012;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8005),tau$alpha$state$map_key_$_iter__8002.call(null,cljs.core.chunk_rest.call(null,s__8003__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8005),null);
}
} else {
var vec__8009 = cljs.core.first.call(null,s__8003__$2);
var k = cljs.core.nth.call(null,vec__8009,(0),null);
var v = cljs.core.nth.call(null,vec__8009,(1),null);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [f.call(null,k),v], null),tau$alpha$state$map_key_$_iter__8002.call(null,cljs.core.rest.call(null,s__8003__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,m);
})());
});
tau.alpha.state.message_handler = (function tau$alpha$state$message_handler(handlers,e){
var port = e.data.port;
var serialized = (e.data["serialized"]);
var deserialized = (function (){try{return tau.alpha.state.deserialize.call(null,serialized);
}catch (e8015){var e__$1 = e8015;
cljs.core.println.call(null,"error:",e__$1);

return cljs.core.println.call(null,"serial:",serialized);
}})();
var tid = cljs.core.keyword.call(null,new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(deserialized));
var data = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(deserialized);
var transferables = tau.alpha.state.map_key.call(null,tau.alpha.state.deserialize,cljs.core.js__GT_clj.call(null,(e.data["transferables"])));
var copied = cljs.core.reduce.call(null,((function (port,serialized,deserialized,tid,data,transferables){
return (function (p1__8013_SHARP_,p2__8014_SHARP_){
return cljs.core.assoc_in.call(null,p1__8013_SHARP_,cljs.core.first.call(null,p2__8014_SHARP_),cljs.core.second.call(null,p2__8014_SHARP_));
});})(port,serialized,deserialized,tid,data,transferables))
,data,transferables);
var copied__$1 = ((cljs.core.not.call(null,port))?copied:cljs.core.assoc.call(null,copied,new cljs.core.Keyword(null,"port","port",1534937262),port));
var temp__5457__auto__ = cljs.core.get.call(null,handlers,tid);
if(cljs.core.truth_(temp__5457__auto__)){
var handler = temp__5457__auto__;
return handler.call(null,copied__$1);
} else {
return null;
}
});
tau.alpha.state.new_port_fns = cljs.core.atom.call(null,cljs.core.PersistentHashSet.EMPTY);
tau.alpha.state.add_listeners = (function tau$alpha$state$add_listeners(handlers){
return self.addEventListener(tau.alpha.state.event_message,cljs.core.partial.call(null,tau.alpha.state.message_handler,handlers));
});

//# sourceMappingURL=state.js.map
